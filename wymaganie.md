Wymagania techniczne systemu„ Nieruchomości”

Skład grupy: Marek Kopeć Przemysław Pietrzak
Wymagania funkcjonalne

UC 1. Założenie konta 

Aktorzy: Sprzedający Kupujący Scenariusz główny: 

1. Użytkownik podaje login, który ma formę adresu email.
2. Użytkownik podaje numer telefonu. 
3. Użytkownik podaję hasło z powtórzeniem. 

Scenariusze alternatywne/rozszerzenia: 

Ad. 1 Użytkownik podaję błędny login (forma loginu nie jest adresem email) – wyświetlenie błędu z komunikatem. 
Ad. 2 Użytkownik podaję błędny numer telefonu (błędna liczba cyfr) – wyświetlenie błędu z komunikatem. 
Ad. 3 Użytkownik podaję hasło, które nie spełnia wymagań lub powtórzenie różni się od pierwszego hasła - wyświetlenie błędu z komunikatem. 

UC 2. Dodanie oferty 
Aktorzy: Sprzedający 

Scenariusz główny: 
1. Dodanie informacji o lokalizacji nieruchomości.
2. Dodanie zdjęcia. 
3. Dodanie informacji o nieruchomości: liczba mieszkań powierzchnia mieszkania, kondygnacja. 
4. Dodanie informacji o obecności balkonu. 
5. Dodanie opisu tekstowego. 

Scenariusze alternatywne/rozszerzenia:

Ad. UC 2. :  Użytkownik błędnie wypełnia formularz.  System prezentuje formularz ze wskazanymi błędami.  Użytkownik poprawia błędy. 

UC 3. Edycja oferty 
Aktorzy: Sprzedający 

Scenariusz główny: 
1. Udostępnienie sprzedającemu możliwość edycji wszystkich pól z 



UC 2. 
Scenariusze alternatywne/rozszerzenia: 
Ad. UC 3. :  Użytkownik wypełnia formularz z błędami, walidacje na pola identyczne jak przy tworzeniu oferty.  System prezentuje formularz ze wskazanymi błędami.  Użytkownik poprawia błędy. 

UC 4. Usunięcie oferty 

Aktorzy: Sprzedający 

Scenariusz główny: 
1. Użytkownik ma możliwość usunięcia oferty z listy swoich ofert, potwierdza oknem dialogowym. 

Scenariusze alternatywne/rozszerzenia: 
Ad. UC 4. :  Użytkownik po próbie usunięcia w oknie dialogowym wybiera „NIE”.  Powrót do listy ofert. 
