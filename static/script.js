(function() {
  var fileBase64;
    function getBase64(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
          document.querySelector('[type="text"][name="photo"]').value = reader.result;
        };
        reader.onerror = function (error) {
          console.log('Error: ', error);
        };
     }
     
     var input = document.querySelector('[type="file"][name="photo_file"]');
     input && input.addEventListener('change', function() {
        var file = input.files[0];
        getBase64(file);
     });
      
})();
