from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.models import User
from django.shortcuts import redirect


def sign_up(request):
    if request.method == 'GET':
        return render(request, 'user/sign_up.html')
    try:
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        
        user = User.objects.create_user(name, email, password)
        user.save()

        messages.success(request, 'User created successfully.')

        return redirect('/accounts/login/')
    except Exception as e:
        messages.error(request, str(e))
        return redirect('/accounts/login/')


def login_view(request):
    if request.method == 'GET':
        return render(request, 'user/sign_in.html')
    elif request.method == 'POST':
        name = request.POST.get('name', '')
        password = request.POST.get('password', '')
        user = authenticate(request, username=name, password=password)
        if user:
            login(request, user)
            return redirect('/list')

        messages.success(request, 'Incorrent name or passowrd')
        return redirect('/accounts/login/')



def logout_view(request):
    logout(request)
    return redirect('/accounts/login/')
