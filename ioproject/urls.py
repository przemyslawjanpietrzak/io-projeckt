from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url

import advertisement.views as advertisement_views
import user.views as user_views 

urlpatterns = [
    path('admin/', admin.site.urls),

    path('add', advertisement_views.create),
    path('list', advertisement_views.list),
    
    path('search', advertisement_views.search),

    path('report', advertisement_views.report),


    path('notify/<int:advertisement_id>', advertisement_views.notify),
    path('notifications/<int:advertisement_id>', advertisement_views.notifications),
    path('sell/<int:advertisement_id>', advertisement_views.sell),

    path('notification/<int:notification_id>', advertisement_views.notification),

    path('item/<int:advertisement_id>', advertisement_views.item),
    path('delete/<int:advertisement_id>', advertisement_views.delete),
    path('edit/<int:advertisement_id>', advertisement_views.edit),


    path('sign_up', user_views.sign_up),
    path('logout', user_views.logout_view),
    path('accounts/login/', user_views.login_view),
    path('logout', user_views.logout_view),

    path('', advertisement_views.list),
]
