from django.db import models
from django.contrib.auth.models import User


class Advertisement(models.Model):

    location = models.CharField(max_length=1023)
    photo = models.CharField(max_length=1023)
    space = models.IntegerField()
    description = models.CharField(max_length=1023)
    includes_balcony = models.BooleanField()
    rooms_count = models.IntegerField()
    is_sold = models.BooleanField()
    price = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Notification(models.Model):

    advertisement = models.ForeignKey(Advertisement, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.CharField(max_length=1023)
    visited = models.BooleanField()


class Trade(models.Model):

    date = models.DateTimeField(auto_now_add=True, blank=True)
    price = models.IntegerField()