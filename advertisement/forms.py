from django import forms
from .models import Advertisement

class AdvertisementFrom(forms.Form):
    location = forms.CharField(label='location')
    photo = forms.CharField(label='photo')
    space = forms.IntegerField(label='space', min_value=0)
    description = forms.CharField(label='description')
    includes_balcony = forms.BooleanField(label='includes_balcony', required=False)
    rooms_count = forms.IntegerField(label='rooms_count', min_value=0)
    price = forms.IntegerField(label='price', min_value=0)
    photo_file = forms.FileField(required=False)


class AdvertisementSearchFrom(forms.Form):

    description = forms.CharField(label='Description', max_length=100)

    price_min = forms.IntegerField(label='Price - min')
    price_max = forms.IntegerField(label='Price - max')

    space_min = forms.IntegerField(label='Space - min')
    space_max = forms.IntegerField(label='Space - max')

    rooms_count_min = forms.IntegerField(label='Rooms - min')
    rooms_count_max = forms.IntegerField(label='Rooms - max')

    includes_balcony = forms.BooleanField(label='Includes balcony', required=False)
