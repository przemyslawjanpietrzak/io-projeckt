from django.contrib import admin

from .models import Advertisement, Notification, Trade

class AdvertisementAdmin(admin.ModelAdmin):
    pass

class NotificationAdmin(admin.ModelAdmin):
    pass

class TradeAdmin(admin.ModelAdmin):
    list_filter = (
        ('date', )
    )
      
admin.site.register(Advertisement, AdvertisementAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(Trade, TradeAdmin)