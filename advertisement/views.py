from .models import Advertisement, Notification, Trade
from .forms import AdvertisementFrom, AdvertisementSearchFrom

from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Avg

import datetime


@login_required
def list(request):
    price_min = request.GET.get('price_min', 0)
    price_max = request.GET.get('price_max', 99999999)

    space_min = request.GET.get('space_min', 0)
    space_max = request.GET.get('space_max', 99999999)

    rooms_count_min = request.GET.get('rooms_count_min', 0)
    rooms_count_max = request.GET.get('rooms_count_max', 99999999)

    includes_balcony = request.GET.get('includes_balcony', None)
    description = request.GET.get('description', '')

    advertisements = (Advertisement
        .objects
        .filter(
            price__lte=price_max,
            price__gte=price_min,
            space__gte=space_min,
            space__lte=space_max,
            rooms_count__lte=rooms_count_max,
            rooms_count__gte=rooms_count_min,
            description__icontains=description,
            is_sold=False
        ))

    return render(request, 'advertisement/list.html', {'advertisements': advertisements})


@login_required
def item(request, advertisement_id):
    advertisement = get_object_or_404(Advertisement, pk=advertisement_id)
    is_user_advertisement = advertisement.user.pk == request.user.pk

    return render(request, 'advertisement/item.html', {
        'advertisement': advertisement,
        'is_own': is_user_advertisement
    })


@login_required
def delete(request, advertisement_id):
    advertisement = get_object_or_404(Advertisement, pk=advertisement_id)
    is_user_advertisement = advertisement.user.pk == request.user.pk

    if advertisement and is_user_advertisement:
        advertisement.delete()
        messages.success(request, 'advertisement was removed.')

    return redirect('/list')


@login_required
def create(request):
    
    if request.method == 'GET':
        form = AdvertisementFrom()
        return render(request, 'advertisement/create.html', {'form': form})

    location = request.POST.get('location')
    photo = request.POST.get('photo')
    space = request.POST.get('space')
    description = request.POST.get('description')
    includes_balcony = request.POST.get('includes_balcony')
    rooms_count = request.POST.get('rooms_count')
    price = request.POST.get('price')

    advertisement = Advertisement(
        location=location,
        photo=photo,
        space=space,
        description=description,
        includes_balcony=includes_balcony == 'on',
        rooms_count=rooms_count,
        price=price,
        is_sold=False,
        user=request.user
    )
    advertisement.save()

    messages.success(request, 'advertisement created successfully.')

    return redirect('/item/{}'.format(advertisement.pk))


@login_required
def sell(request, advertisement_id):
    if request.method == 'POST':
        advertisement = Advertisement.objects.get(pk=advertisement_id)
        advertisement.is_sold = True
        advertisement.save()
        trade = Trade(price=advertisement.price)
        trade.save()
        messages.success(request, 'sold success')

        return redirect('/list')



@login_required
def search(request):
    form = AdvertisementSearchFrom(initial={
        'description': '',
        'price_min': 0,
        'price_max': 9999999999,

        'space_min': 0,
        'space_max': 9999999999,

        'rooms_count_min': 0,
        'rooms_count_max': 9999999999,

    })
    return render(request, 'advertisement/search.html', {'form': form})


@login_required
def notify(request, advertisement_id):
    if request.method == 'GET':
        return render(request, 'advertisement/notify.html')
    if request.method == 'POST':
        content = request.POST.get('content')
        advertisement = Advertisement.objects.get(pk=advertisement_id)

        notification = Notification(
            user=request.user,
            advertisement=advertisement,
            content=content,
            visited=False
        )
        notification.save()

        return redirect('/item/{}'.format(advertisement_id))


@login_required
def notifications(request, advertisement_id):
    notifications = Notification.objects.filter(advertisement__pk=advertisement_id)
    
    return render(request, 'advertisement/notifications.html', {'notifications': notifications[::-1]})


@login_required
def notification(request, notification_id):
    if request.method == 'GET':
        notification = Notification.objects.get(pk=notification_id)
        if not notification.visited:
            notification.visited = True
            notification.save()
        
        return render(request, 'advertisement/notification.html', {'notification': notification})
    if request.method == 'POST':
        notification = Notification.objects.get(pk=notification_id)
        if request.user.pk == notification.advertisement.user.pk:
            notification.delete()
            messages.success(request, 'Notification was removed.')

            return redirect('/list')


@login_required
def edit(request, advertisement_id):
    if request.method == 'GET':
        advertisement = get_object_or_404(Advertisement, pk=advertisement_id)

        form = AdvertisementFrom(initial={
            'location': advertisement.location,
            'photo': advertisement.photo,
            'space': advertisement.space,
            'description': advertisement.description,
            'includes_balcony': advertisement.includes_balcony,
            'rooms_count': advertisement.rooms_count,
            'price': advertisement.price,
        })

        return render(request, 'advertisement/edit.html', {'form': form, 'advertisement': advertisement})
    else:
        location = request.POST.get('location')
        photo = request.POST.get('photo')
        space = request.POST.get('space')
        description = request.POST.get('description')
        includes_balcony = request.POST.get('includes_balcony') == 'on'
        rooms_count = request.POST.get('rooms_count')
        price = request.POST.get('price')

        advertisement = get_object_or_404(Advertisement, pk=advertisement_id)

        advertisement.location = location 
        advertisement.photo = photo 
        advertisement.space = space 
        advertisement.description = description 
        advertisement.includes_balcony = includes_balcony 
        advertisement.rooms_count = rooms_count 
        advertisement.price = price 

        advertisement.save()

        return redirect('/item/{}'.format(advertisement.pk))


@user_passes_test(lambda u: u.is_superuser)
def report(request):
    today = datetime.date.today()
    trades = Trade.objects.filter(date__gte=today - datetime.timedelta(days=1))

    count = trades.count()
    avg = trades.aggregate(Avg('price'))['price__avg']

    return render(request, 'advertisement/report.html', {'count': count, 'avg': avg})
 

